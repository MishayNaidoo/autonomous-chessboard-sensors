# Autonomous Chessboard Sensors


## Project Summary
Project for the creation of a HAT for an STM32 Discovery Board that is used for detecting magnetic fields and temperature sensing. The main purpose of the HAT is to assist in the creation of an autonomous chessboard, however, other uses can be found for the HAT. This Gitlab project includes all of the CAD files, documents, firmware, PCB's and simulations used to create the HAT.

## Navigating the GIT Repo

The KiCad project files can be located in the "KiCad Files" folder. This includes the schematics of each of the subsciruits as well as the PCB layout. All the footprints for each of the componants used can be found in the "Footprints Folder". Similarly the datasheets of the components can be found in the "Datasheets" folder contained in the resources folder. The gerber files can be found in the "Gerbers" folder. Finally the bill of materials can be found in the "BOM_Group 6.xlsx" folder.


## Hardware

This is what you will need in terms of hardware:
- STM32F051R8T6
- Rowdy Chess HAT
- MicroUSB to USB cable
- USB to USB-D
- Digital Device with Serial (Laptop/Computer device)
- Li_Ion battery 18650

1) To start, connect the STM to your Digital Device, you should see flashing LEDs on the STM
2) Power the Hat with the USB and plug it into the STM Discovery Board. (You can also power the HAT with just the battery)
3) Attach the HAT to the STM with the GPIO pins.
4) You can now use the Digital Device to write sofware to the STM Discovery Board.

## Software

- Access software in Resources -> STM Software
- Use any IDE capable of interfacing with STM32F051R8T6. STM32CubeIDE is recommended.
- Serial port monitoring software, eg. PuTTY.
- Download software and build it in your IDE.
- Upload software to the STM Discovery Board through your serial port.

- Now disconnect your STM from your device.
- To view data being received by HAT, open serial port connection on your digital device and display serial. 
- Once you reconnect your STM, "USB Detected" will be displayed to confirm everything is working.
- Now your data will be displayed every 4 seconds.

## License

Creative Commons. 
(Find in LICENSE FOLDER)

## Authors and acknowledgment
Project Members:
- David Newton 
- Mishay Naidoo
- Daniel Jones


