# Contributing

## Contacts:

Daniel Jones (jnsdan013@myuct.ac.za)

David Newton (nwtdav002@myuct.ac.za)

Mishay Naidoo (ndxmis011@myuct.ac.za)

## Policy on Contributing

Contributing to this project is encouraged. Contributions to both the hardware and software elements are welcome. Simply create a pull request.

## Contributing Guidelines

### Software Contribution

Ensure all code is commented and formatted in the same manner of the automatically generated "main.c" file created by the "STM32CubeIDE" software. 

### Hardware Contribution

Ensure all added hardware to schematics is labelled and does not cause the HAT to operate out of desired operating conditions with regards to size and power draw.

### Merging 

If you wish to merge your contribution with the main project, please create a merge request. Your merge request must be accompanied with a report (pdf format) detailing the changes you have made and in depth descriptions of the problems you have fixed or features that you have added.

All merge requests will be reviewed in 2 to 3 business days.

### Bug Fixes 

Any potential bugs found must be reported to one of the email addresses shown in "contacts".

## Code of Conduct

This project was created by largely inexperienced electrical engineering undergrads with limited free time. When contributing please be patient, the creators of this project will get back to you as soon as possible.

