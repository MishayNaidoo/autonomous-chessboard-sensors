*PADS-LIBRARY-PART-TYPES-V9*

AH1902-Z-7 AH1902Z7 I ANA 9 1 0 0 0
TIMESTAMP 2022.03.17.13.25.53
"Manufacturer_Name" Diodes Inc.
"Manufacturer_Part_Number" AH1902-Z-7
"Mouser Part Number" 621-AH1902-Z-7
"Mouser Price/Stock" https://www.mouser.com/Search/Refine.aspx?Keyword=621-AH1902-Z-7
"Arrow Part Number" 
"Arrow Price/Stock" 
"Description" Board Mount Hall Effect / Magnetic Sensors Omni Hall-Effect 1.6V to 3.6V 4.3uA
"Datasheet Link" https://componentsearchengine.com/Datasheets/1/AH1902-Z-7.pdf
"Geometry.Height" 0.62mm
GATE 1 5 0
AH1902-Z-7
1 0 U NC_1
2 0 U GND
3 0 U NC_2
4 0 U VDD
5 0 U OUTPUT

*END*
*REMARK* SamacSys ECAD Model
1054156/789157/2.49/5/4/Integrated Circuit
